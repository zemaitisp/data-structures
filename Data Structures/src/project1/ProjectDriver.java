package project1;
import java.util.*;
/**
 * Will create a linked list of type book and interact with the user 
 * @author Peter Zemaitis
 *
 */
public class ProjectDriver 
{
	private static Scanner scan = new Scanner(System.in);
	private static LinkedBag<Book> bookBag = new LinkedBag<Book>();
	
	/**
	 * Main method where the user interface is 
	 * @param args
	 */
	public static void main(String[] args) 
	{
		String option;
		boolean done = false;
		do
		{
			System.out.println("Please enter one of the following\n");
			System.out.println("A - Add Book to bag \nR - Remove Book from bag \nF - find if a book is in the bag");
			System.out.println("C - Counts the number of books in the bag \nD - display the contents of the bag");
			System.out.println("S - Display the size of the bag \nX - Exit the program");
			option = scan.nextLine();
			switch(option.toLowerCase())
			{
				case "a":
				{
					addToBag();
					break;
				}
				case "r":
				{
					removeFromBag();
					break;
				}
				case "f":
				{
					findInBag();
					break;
				}
				case "c":
				{
					countInBag();
					break;
				}
				case "d":
				{
					displayContent();
					break;
				}
				case "s":
				{
					System.out.println("The size of the bag is " + bookBag.getSize());
					break;
				}
				case "x":
				{
					done = true;
					break;
				}
				default:
				{
					System.out.println("You entered an incorrect option");
					break;
				}
			}	
		}
		while(!done);
	}
	
	/**
	 * Used in many methods
	 * Will create a book obj with the data input by the user
	 * @return Book obj with data inputted by the user
	 */
	private static Book createBook()
	{
		String inputName;
		int inputYear;
		System.out.println("Please enter the book name you want added");
		inputName = scan.nextLine();
		System.out.println("Please enter the year the book was published");
		inputYear = scan.nextInt();
		scan.nextLine();
		return new Book(inputName, inputYear);
	}
	
	/**
	 * add a book to the linked list
	 */
	private static void addToBag()
	{
		bookBag.add(createBook());
	}
	
	/**
	 * will remove a book from the linked list
	 */
	private static void removeFromBag()
	{
		boolean removed = bookBag.remove(createBook());
		if(removed)
		{
			System.out.println("Your book was removed");
		}
		else
		{
			System.out.println("Your book could not be found");
		}
	}
	
	/**
	 * will atempt to find the book in the linked list
	 */
	private static void findInBag()
	{
		boolean found = bookBag.exists(createBook());
		if(found)
		{
			System.out.println("Book was found in the bag");
		}
		else
		{
			System.out.println("Book was not found in bag");
		}
	}

	/**
	 * will count the number of same book objs in the linked list
	 */
	private static void countInBag()
	{
		System.out.println("There were " + bookBag.countOccurreneces(createBook()) + " instances of the book in the bag");
	}
	
	/**
	 * Will be used to display the contents of the bag in place of toString
	 */
	private static void displayContent()
	{
		  System.out.print(": [" );


	      if (bookBag.getSize() == 0)
	          System.out.print("Empty");
	      else
	      {
	          Lister<Book> bookList = bookBag.iterator();
	          while (bookList.hasNext())
	          {
	              Book displayBook = bookList.next();
	              System.out.print("{Name:" + displayBook.getBookName() + " | Year Published:" + displayBook.getYearPublished() + "}");
	              if (bookList.hasNext())
	                  System.out.print( ", ");
	          }
	      }
	}
	
}
