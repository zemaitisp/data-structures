package project1;
import java.util.*;
/**
 * Will create a obj that holds a single node. Used to get the data out of a node easily. Used in place of the toString method
 * @author Peter Zemaitis
 *
 * @param <E>
 */
public class Lister<E> implements Iterator<E> 
{
	private Node<E> current;
	
	/**
	 * Will create a lister with the current set to a given node
	 * @param head
	 */
	public Lister (Node<E> head)
	{
		current = head;
	}

	/**
	 * return true if there is another node in the list, false otherwise
	 */
	public boolean hasNext() 
	{
		return (current != null);
	}

	/**
	 * if it does not have another node in the lister then it will throw a exception 
	 * else it will get the next node in the list and return the old one
	 */
	public E next() 
	{
		E answer;
		
		if(!hasNext())
		{
			throw new NoSuchElementException("The lister is empty");
		}
		answer = current.getData();
		current = current.getLink();
		
		return answer;
	}
	
	/**
	 * Do Not Use
	 */
	public void remove()
	{
		throw new UnsupportedOperationException("Lister has no remove method");
	}

}
