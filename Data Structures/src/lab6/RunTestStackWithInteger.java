package lab6;
/**
    Programmed by   Stephen Brower
    Inspired by     Michael Main
    Date Written    10/3/2014 - written to use generic tester to test generic linked list
                                needs TestLinkedBagGeneric.java

    Date Modified   10/12/2014 - changed to use stacks
    *               3/16/2017 - changed inputs, updated output

    For Sample Output, see below
*/

public class RunTestStackWithInteger
{
    public static void main(String[] args)
    {
        runMe();
    }

    public static void runMe()
    {
        TestStackGeneric<Integer> testStackInteger = new TestStackGeneric<Integer>();

        Integer[] arrayOfIntsToAdd = {8, 6, 7, 5, 3, 0, 9};

        testStackInteger.test ( arrayOfIntsToAdd);
    }

}

/*
Expected output:
run:

Display Stack Size on startup
Size: 0 -good

===========
<<Start Pushes:
Pushing: 8  pushed...now size is 1 -good
Pushing: 6  pushed...now size is 2 -good
Pushing: 7  pushed...now size is 3 -good
Pushing: 5  pushed...now size is 4 -good
Pushing: 3  pushed...now size is 5 -good
Pushing: 0  pushed...now size is 6 -good
Pushing: 9  pushed...now size is 7 -good
Stopped Pushing>>
===========


Display stack size after adds
Size: 7 -good

pop?...popped: <9> exp:{9}  size: 6 -good

Display stack size after 1 pop
Size: 6 -good

===========
<<Start popping:
pop?...popped: <0> exp:{0}  size: 5 -good
pop?...popped: <3> exp:{3}  size: 4 -good
pop?...popped: <5> exp:{5}  size: 3 -good
pop?...popped: <7> exp:{7}  size: 2 -good
pop?...popped: <6> exp:{6}  size: 1 -good
pop?...popped: <8> exp:{8}  size: 0 -good
Stopped Popping>>
===========

Display stack size after pop all
Size: 0 -good

attempt to remove item from empty stack:
pop? - EmptyStack Exception - Can't pop stack  -good

Display stack size after 1 pop
Size: 0 -good
BUILD SUCCESSFUL (total time: 0 seconds)

*/