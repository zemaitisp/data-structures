package lab6;
/**
 * Simple test of stack using ints/Integer
 * Date Written     6/27/2013
 * Date Modified    3/16/2017 - Netbeanafied, added size on push
 *                              added try/catch for EmptyStack
 *                              and added an extra pop which should result a failed pop
 * @author Stephen T. Brower<stephen.brower@raritanval.edu>
 */

public class SimpleStackOfIntegerTest
{
    public static void main(String[] args)
    {
        // creates a stack of Integer
        Stack<Integer> myIntStack = new Stack<Integer>();  // Stack of Integer!

        int anInteger;

        System.out.println("Starting: Size is " + myIntStack.size());

        // add some ints to Stack
        anInteger = 8;
        System.out.print("Pushing " + anInteger);
        myIntStack.push(anInteger);
        System.out.println("...pushed.  size=" + myIntStack.size());

        anInteger = 6;
        System.out.print("Pushing " + anInteger);
        myIntStack.push(anInteger);
        System.out.println("...pushed.  size=" + myIntStack.size());

        anInteger = 7;
        System.out.print("Pushing " + anInteger);
        myIntStack.push(anInteger);
        System.out.println("...pushed.  size=" + myIntStack.size());

        anInteger = 5;
        System.out.print("Pushing " + anInteger);
        myIntStack.push(anInteger);
        System.out.println("...pushed.  size=" + myIntStack.size());

        anInteger = 3;
        System.out.print("Pushing " + anInteger);
        myIntStack.push(anInteger);
        System.out.println("...pushed.  size=" + myIntStack.size());

        anInteger = 0;
        System.out.print("Pushing " + anInteger);
        myIntStack.push(anInteger);
        System.out.println("...pushed.  size=" + myIntStack.size());

        anInteger = 9;
        System.out.print("Pushing " + anInteger);
        myIntStack.push(anInteger);
        System.out.println("...pushed.  size=" + myIntStack.size());


        System.out.println("\nPop ALL Items from Stack");
        // does stack have items?
        while ( myIntStack.size() > 0 )
        {
            try
            {
                System.out.print("attempting to pop...");
                // yes, get the item on top
                anInteger = myIntStack.pop();  // gets the 1 item on top of the stack

                // display that 1 Car from the top
                System.out.println("Popped: " + anInteger +" size=" + myIntStack.size());
            }
            catch (EmptyStack e)
            {
                System.out.println("Can't pop stack - empty\t\t<=== Issue");
            }
        }

        // let's try one more pop
        try
        {
            System.out.print("\nattempting to pop...");
            // yes, get the item on top
            anInteger = myIntStack.pop();  // gets the 1 item on top of the stack

            // display that 1 Car from the top
            System.out.println("Popped: " + anInteger +"\t\t<=== Issue!/nsize=" + myIntStack.size());
        }
        catch (EmptyStack e)
        {
            System.out.println("\nCan't pop stack - empty-good");
        }

    }

}

/*
Sample Output is:
run:
Starting: Size is 0
Pushing 8...pushed.  size=1
Pushing 6...pushed.  size=2
Pushing 7...pushed.  size=3
Pushing 5...pushed.  size=4
Pushing 3...pushed.  size=5
Pushing 0...pushed.  size=6
Pushing 9...pushed.  size=7

Pop ALL Items from Stack
attempting to pop...Popped: 9 size=6
attempting to pop...Popped: 0 size=5
attempting to pop...Popped: 3 size=4
attempting to pop...Popped: 5 size=3
attempting to pop...Popped: 7 size=2
attempting to pop...Popped: 6 size=1
attempting to pop...Popped: 8 size=0

attempting to pop...
Can't pop stack - empty-good
BUILD SUCCESSFUL (total time: 0 seconds)


*/