package lab6;
/**
 * Simple test of queue using ints/Integer
 * Date Written:    10/23/2012
 * Date Modified    3/27/2013 - renamed to Lab 6/7
 *                  7/11/2013 - renamed to Lab 8
 *                  3/16/2017 - Netbeanafied, added size to adds/removes
 *                              added try/catch for EmptyQueue
 *                              and added an extra remove which should result a failed remove
 * @author Stephen T. Brower<stephen.brower@raritanval.edu>
 */

public class SimpleQueueOfIntegerTest
{
    public static void main(String[] args)
    {
        Queue<Integer> queueOfInts = new Queue<Integer>();
        int anInteger;


        System.out.println("Starting: Size is " + queueOfInts.size());


        anInteger = 8;
        System.out.print("Adding: " + anInteger);
        queueOfInts.add(anInteger);
        System.out.println("...added.  size=" + queueOfInts.size());


        anInteger = 6;
        System.out.print("Adding: " + anInteger);
        queueOfInts.add(anInteger);
        System.out.println("...added.  size=" + queueOfInts.size());

        anInteger = 7;
        System.out.print("Adding: " + anInteger);
        queueOfInts.add(anInteger);
        System.out.println("...added.  size=" + queueOfInts.size());

        anInteger = 5;
        System.out.print("Adding: " + anInteger);
        queueOfInts.add(anInteger);
        System.out.println("...added.  size=" + queueOfInts.size());

        anInteger = 3;
        System.out.print("Adding: " + anInteger);
        queueOfInts.add(anInteger);
        System.out.println("...added.  size=" + queueOfInts.size());

        anInteger = 0;
        System.out.print("Adding: " + anInteger);
        queueOfInts.add(anInteger);
        System.out.println("...added.  size=" + queueOfInts.size());

        anInteger = 9;
        System.out.print("Adding: " + anInteger);
        queueOfInts.add(anInteger);
        System.out.println("...added.  size=" + queueOfInts.size());


        System.out.println("\nAfter adds: Size is " + queueOfInts.size());

        System.out.println("\nItems from Queue");
        // does Queue have items?
        while ( queueOfInts.size() > 0 )
        {
            try
            {
                System.out.print("attempting to take from queue...");
                anInteger = queueOfInts.remove();
                // display 1 item
                System.out.println("...removed: " + anInteger
                            + "\tsize: " + queueOfInts.size());

            }
            catch (EmptyQueue e)
            {
                System.out.println("Can't remove from Queue - empty");
            }

        }


        // let's try one more remove
        try
        {
            System.out.print("\nattempting to remove...");
            // yes, get the item on top
            anInteger = queueOfInts.remove();  // gets the 1 item on top of the stack

            // display that 1 item
            System.out.println("removed: " + anInteger +"\t\t<=== Issue!\nsize=" + queueOfInts.size());
        }
        catch (EmptyQueue e)
        {
            System.out.println("\nCan't remove from queue - empty - good");
        }

    }
}

/* Output is:
run:
Starting: Size is 0
Adding: 8...added.  size=1
Adding: 6...added.  size=2
Adding: 7...added.  size=3
Adding: 5...added.  size=4
Adding: 3...added.  size=5
Adding: 0...added.  size=6
Adding: 9...added.  size=7

After adds: Size is 7

Items from Queue
attempting to take from queue......removed: 8   size: 6
attempting to take from queue......removed: 6   size: 5
attempting to take from queue......removed: 7   size: 4
attempting to take from queue......removed: 5   size: 3
attempting to take from queue......removed: 3   size: 2
attempting to take from queue......removed: 0   size: 1
attempting to take from queue......removed: 9   size: 0

attempting to remove...
Can't remove from queue - empty - good
BUILD SUCCESSFUL (total time: 0 seconds)


*/