package lab6;

/**
 * 
 * @author Peter
 *Will create a queue for one type of obj
 * @param <E> obj you are working with
 */
public class Queue <E>
{
	private Node<E> front;
	private Node<E> rear;
	private int numElements;
	
	/**
	 *  no args constructor that set front and rear to null and number of elements to zero
	 */
	public Queue()
	{
		front = null;
		rear = null;
		numElements = 0;
	}
	
	/**
	 * Will add an obj to the rear of the queue
	 * @param element obj added to queue
	 */
	public void add(E element)
	{
		if(front == null)
		{
			front = new Node<E>(element, null);
			rear = front;
		}
		else
		{
			rear.setLink(new Node<E>(element, null));
			rear = rear.getLink();
		}
		numElements++;
	}
	
	/**
	 * will attempt to remove an element from the queue
	 * @return element you are looking to remove
	 * @throws EmptyQueue will occure if the queue is empty
	 */
	public E remove() throws EmptyQueue
	{
		E toReturn = null;
		if(front != null)
		{
			toReturn = front.getData();
			front = front.getLink();
			numElements--;
		}
		else
		{
			throw new EmptyQueue();
		}
		return toReturn;
	}
	
	/**
	 * will return the length of the queue
	 * @return length of the queue
	 */
	public int size()
	{
		return numElements;
	}
}
