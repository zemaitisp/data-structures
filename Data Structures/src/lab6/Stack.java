package lab6;
/**
 * 
 * @author Peter
 * stack class that will hold one type of object
 * @param <E> type of obj the stack will hold
 */
public class Stack <E>
{
	private Node<E> top;
	private int numElements;
	
	/**
	 * constructor that sets top to null and the number of elements to zero
	 */
	public Stack()
	{
		top = null;
		numElements = 0;
	}
	
	/**
	 * will add a new obj to the top of the class
	 * @param element
	 */
	public void push(E element)
	{
		if(top == null)
		{
			top = new Node<E>(element, null);
		}
		else
		{
			top = new Node<E>(element, top);
		}
		numElements++;
	}
	
	/**
	 * will return the obj at the top of the stacl
	 * @return obj at the top of the stack
	 * @throws EmptyStack will occure if the stack is empty
	 */
	public E pop() throws EmptyStack
	{
		E toReturn = null;
		if(top != null)
		{
			toReturn = top.getData();
			top = top.getLink();
			numElements--;
		}
		else
		{
			throw new EmptyStack();
		}
		return toReturn;
	}
	
	/**
	 * return the length of the stack
	 * @return size of the stack
	 */
	public int size()
	{
		return numElements;
	}
	
	
}
