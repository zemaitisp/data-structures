package lab6;

/**
 *
 * @author Stephen T. Brower<stephen.brower@raritanval.edu>
 */
public class Lab06StacksQueuesTesterS17 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        System.out.println("Either run these 4 simple testers and visually inspect the output:");
        System.out.println("\tSimpleQueueOfCarTest");
        System.out.println("\tSimpleQueueOfIntegerTest");
        System.out.println("\tSimpleStackOfCarTest");
        System.out.println("\tSimpleStackOfIntegerTest");
        System.out.println("\nOr, run these 4 testers and look for the flag '<=== issue':");
        System.out.println("\tRunTestQueueWithCar.java");
        System.out.println("\tRunTestQueueWithInteger.java");
        System.out.println("\tRunTestStackWithCar.java");
        System.out.println("\tRunTestStackWithInteger.java");
    }

}
