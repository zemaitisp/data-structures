package lab6;

/**
 * Simple test of queue using Car
 * Date Written:    3/20/2012
 * Date Modified    10/23/2012 - removed colorado package info
 *                               renamed to lab 8
 *                  3/27/2013 - renamed to Lab 6/7
 *                  7/11/2013 - renamed to Lab 8
 *                  3/16/2017 - Netbeanafied, added size to adds/removes
 *                              added try/catch for EmptyQueue
 *                              and added an extra remove which should result a failed remove
 * @author Stephen T. Brower<stephen.brower@raritanval.edu>
 */

public class SimpleQueueOfCarTest
{
    public static void main(String[] args)
    {
        Queue<Car> myCarQueue = new Queue<Car>();

        Car aCar;                     // for adding to queue

        System.out.println("Starting: Size is " + myCarQueue.size());


        // add some cars to Stack
        aCar = new Car(2013,"Honda Accord");
        System.out.print("Adding " + aCar);
        myCarQueue.add(aCar);
        System.out.println("...added.  size=" + myCarQueue.size());

        aCar =  new Car(1998,"Camry");
        System.out.print("Adding " + aCar);
        myCarQueue.add(aCar);
        System.out.println("...added.  size=" + myCarQueue.size());

        aCar =  new Car(1994,"Explorer");
        System.out.print("Adding " + aCar);
        myCarQueue.add(aCar);
        System.out.println("...added.  size=" + myCarQueue.size());

        aCar =  new Car(2013,"Zoom Zoom");
        System.out.print("Adding " + aCar);
        myCarQueue.add(aCar);
        System.out.println("...added.  size=" + myCarQueue.size());

        aCar =  new Car(2013,"Acura");
        System.out.print("Adding " + aCar);
        myCarQueue.add(aCar);
        System.out.println("...added.  size=" + myCarQueue.size());

        aCar =  new Car(2007,"Neon");
        System.out.print("Adding " + aCar);
        myCarQueue.add(aCar);
        System.out.println("...added.  size=" + myCarQueue.size());

        System.out.println("\nItems from Queue");

        // does Queue have items?
        while ( myCarQueue.size() > 0 )
        {
            try
            {
                System.out.print("attempting to remove...");
                // yes, get an item from the queue
                aCar = myCarQueue.remove(); // gets the 1 item in front of th queue

                // display that 1 Car from the top
                System.out.println("Removed from Queue: " + aCar +" size=" + myCarQueue.size());
            }
            catch (EmptyQueue e)
            {
                System.out.println("Can't pop stack - empty\t\t<=== Issue");
            }
        }

        // let's try one more remove
        try
        {
            System.out.print("\nattempting to remove...");
            // yes, get the item on top
            aCar = myCarQueue.remove(); // gets the 1 item from the front of queue

            // display that 1 Car
            System.out.println("removed: " + aCar + "\t\t<=== Issue");
            System.out.println("size=" + myCarQueue.size());
        }
        catch (EmptyQueue e)
        {
            System.out.println("\nCan't remove from queue - empty - good");
        }


    }
}

/* Output is:
run:
Starting: Size is 0
Adding [Car 2013 Honda Accord]...added.  size=1
Adding [Car 1998 Camry]...added.  size=2
Adding [Car 1994 Explorer]...added.  size=3
Adding [Car 2013 Zoom Zoom]...added.  size=4
Adding [Car 2013 Acura]...added.  size=5
Adding [Car 2007 Neon]...added.  size=6

Items from Queue
attempting to remove...Removed from Queue: [Car 2013 Honda Accord] size=5
attempting to remove...Removed from Queue: [Car 1998 Camry] size=4
attempting to remove...Removed from Queue: [Car 1994 Explorer] size=3
attempting to remove...Removed from Queue: [Car 2013 Zoom Zoom] size=2
attempting to remove...Removed from Queue: [Car 2013 Acura] size=1
attempting to remove...Removed from Queue: [Car 2007 Neon] size=0

attempting to remove...
Can't remove from queue - empty - good
BUILD SUCCESSFUL (total time: 0 seconds)


*/