package lab6;
/**
    Programmed by   Stephen Brower
    Inspired by     Michael Main
    Date Written    10/3/2014 - written to use generic tester to test generic linked list
                                needs TestQueueGeneric.java

    Date Modified   10/12/2014 - changed to use Queue
    *               3/16/2017 - changed inputs, updated output

    For Sample Output, see below
*/

public class RunTestQueueWithInteger
{
    public static void main(String[] args)
    {
        runMe();
    }

    public static void runMe()
    {
        TestQueueGeneric<Integer> testQueueInteger = new TestQueueGeneric<Integer>();

        Integer[] arrayOfIntsToAdd = {8, 6, 7, 5, 3, 0, 9};

        testQueueInteger.test ( arrayOfIntsToAdd);
    }

}

/*
Expected output:
run:

Display Queue Size on startup
Size: 0 -good

===========
<<Start adds:
Adding: 8   added...now size is 1 -good
Adding: 6   added...now size is 2 -good
Adding: 7   added...now size is 3 -good
Adding: 5   added...now size is 4 -good
Adding: 3   added...now size is 5 -good
Adding: 0   added...now size is 6 -good
Adding: 9   added...now size is 7 -good
Stopped adding>>
===========


Display Queue size after adds
Size: 7 -good

About to remove 1 item......removed: <8> exp:{8}    size: 6 -good

Display Queue size after 1 remove
Size: 6 -good

===========
<<Start removing:
About to remove......removed: <6> exp:{6}   size: 5 -good
About to remove......removed: <7> exp:{7}   size: 4 -good
About to remove......removed: <5> exp:{5}   size: 3 -good
About to remove......removed: <3> exp:{3}   size: 2 -good
About to remove......removed: <0> exp:{0}   size: 1 -good
About to remove......removed: <9> exp:{9}   size: 0 -good
Stopped removing>>
===========

Display Queue size after remove all
Size: 0 -good

attempt to remove 1 item from an empty Queue:
remove?Can't remove  - EmptyQueue exception  -good

Display Queue size after 1 remove
Size: 0 -good
BUILD SUCCESSFUL (total time: 0 seconds)



*/