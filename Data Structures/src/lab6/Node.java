package lab6;

/**
 * Will create a node that will store a data type and point to the next 
 * node in the list
 * @author Peter Zemaitis
 *
 * @param <E> data type this node will hold
 */
public class Node<E>
{
	private E data;
	private Node<E> link;
	
	/**
	 * Constructor that will data to a given obj and the link to the next link in the list
	 * @param newData data the node will hold	
	 * @param newLink link to the next node
	 */
	public Node(E newData, Node<E> newLink)
	{
		data = newData;
		link = newLink;
	}
	
	/**
	 * Will set the data held in the node to a different data
	 * @param newData new data the node will hold
	 */
	public void setData(E newData)
	{
		data = newData;
	}
	
	/**
	 * Will set the link to a new node
	 * @param newLink new node that this node will point to
	 */
	public void setLink(Node<E> newLink)
	{
		link = newLink;
	}
	
	/**
	 * Return the data held by the node
	 * @return data held by the node
	 */
	public E getData()
	{
		return data;
	}
	
	/**
	 * return the next node in the list
	 * @return next node in the list
	 */
	public Node<E> getLink()
	{
		return link;
	}
}
