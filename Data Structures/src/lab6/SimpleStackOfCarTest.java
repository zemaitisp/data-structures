package lab6;
/**
 * A simple test of the Stack - you need to visually inspect the output
 * Date Modified    10/2/2012 - fixed the labels to match the actions
 *                  2/20/2013 - Changed Class Name
 *                  6/20/2013 - changed class name
 *                  3/16/2017 - Netbeanafied, added size to pushes and pops
 *                              added try/catch for EmptyStack
 *                              and added an extra pop which should result a failed pop
 * @author Stephen T. Brower<stephen.brower@raritanval.edu> inspired by Michael Main
 */


public class SimpleStackOfCarTest
{
    public static void main(String[] args)
    {
        Car aCar;
        // creates a stack of car
        Stack<Car> myCarStack = new Stack<Car>();

        System.out.println("Starting Size: " + myCarStack.size());

        // add some cars to Stack
        aCar = new Car(2013,"Honda Accord");
        System.out.print("Pushing " + aCar);
        myCarStack.push(aCar);
        System.out.println("...pushed.  size=" + myCarStack.size());

        aCar =  new Car(1998,"Camry");
        System.out.print("Pushing " + aCar);
        myCarStack.push(aCar);
        System.out.println("...pushed.  size=" + myCarStack.size());

        aCar =  new Car(1994,"Explorer");
        System.out.print("Pushing " + aCar);
        myCarStack.push(aCar);
        System.out.println("...pushed.  size=" + myCarStack.size());

        aCar =  new Car(2013,"Zoom Zoom");
        System.out.print("Pushing " + aCar);
        myCarStack.push(aCar);
        System.out.println("...pushed.  size=" + myCarStack.size());

        aCar =  new Car(2013,"Acura");
        System.out.print("Pushing " + aCar);
        myCarStack.push(aCar);
        System.out.println("...pushed.  size=" + myCarStack.size());

        aCar =  new Car(2007,"Neon");
        System.out.print("Pushing " + aCar);
        myCarStack.push(aCar);
        System.out.println("...pushed.  size=" + myCarStack.size());


        System.out.println("\nPop all items on Stack");
        // does stack have items?
        while ( myCarStack.size() > 0 )
        {
            try
            {
                System.out.print("attempting to pop...");
                // yes, get the item on top
                aCar = myCarStack.pop(); // gets the 1 item on top of the stack

                // display that 1 Car from the top
                System.out.println("Popped: " + aCar +" size=" + myCarStack.size());
            }
            catch (EmptyStack e)
            {
                System.out.println("Can't pop stack - empty\t\t<=== Issue");
            }
        }


        // let's try one more pop
        try
        {
            System.out.print("\nattempting to pop...");
            // yes, get the item on top
            aCar = myCarStack.pop(); // gets the 1 item on top of the stack

            // display that 1 Car from the top
            System.out.println("Popped: " + aCar + "\t\t<=== Issue");
            System.out.println("size=" + myCarStack.size());
        }
        catch (EmptyStack e)
        {
            System.out.println("\nCan't pop stack - empty - good");
        }

    }
}

/*
Sample Output is:

run:
Starting Size: 0
Pushing [Car 2013 Honda Accord]...pushed.  size=1
Pushing [Car 1998 Camry]...pushed.  size=2
Pushing [Car 1994 Explorer]...pushed.  size=3
Pushing [Car 2013 Zoom Zoom]...pushed.  size=4
Pushing [Car 2013 Acura]...pushed.  size=5
Pushing [Car 2007 Neon]...pushed.  size=6

Pop all items on Stack
attempting to pop...Popped: [Car 2007 Neon] size=5
attempting to pop...Popped: [Car 2013 Acura] size=4
attempting to pop...Popped: [Car 2013 Zoom Zoom] size=3
attempting to pop...Popped: [Car 1994 Explorer] size=2
attempting to pop...Popped: [Car 1998 Camry] size=1
attempting to pop...Popped: [Car 2013 Honda Accord] size=0

attempting to pop...
Can't pop stack - empty - good
BUILD SUCCESSFUL (total time: 0 seconds)


*/