package lab1;


/**
 * Ball class for Lab 01 M/R
 * @author Stephen Brower
 */
public class Ball 
{
    private double radius;
    private int x, y;
    
    /**
     * Constructor
     * @param initialRadius value for radius field
     * @param initialX value for x field
     * @param initialY value for y field
     */
    public Ball(double initialRadius, int initialX, int initialY)
    {
        radius = initialRadius;
        x = initialX;
        y = initialY;
    }
    
    /**
     * setX method assigns a new value for the x field
     * @param newX value for x field
     */
    public void setX(int newX)
    {
        x = newX;
    }    

    /**
     * setY method assigns a new value for the y field
     * @param newY value for y field
     */
    public void setY(int newY)
    {
        y = newY;
    }    
    
    /**
     * setRadius method assigns a new value for the radius field
     * @param newRadius new value for radius field
     */
    public void setRadius(double newRadius)
    {
        radius = newRadius;
    }    
    /**
     * getX method returns value in x field
     * @return value in x field
     */
    public int getX()
    {
        return x;
    }

    /**
     * getY method returns value in y field
     * @return value in y field
     */
    public int getY()
    {
        return y;
    }

    /**
     * getRadius method returns value in radius field
     * @return value in radius field
     */
    public double getRadius()
    {
        return radius;
    }
    
    /**
     * toString method returns a textual equivalent of this Ball
     * @return textual equivalent of this Ball
     */
    public String toString()
    {
        return "Ball of radius " + radius + " @ position "+
                x + ", " + y;
    }
    
}

