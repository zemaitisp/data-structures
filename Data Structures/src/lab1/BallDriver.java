package lab1;


/**
 * modified by:         *** your name here ***
 * "driver" for the Ball class
 * @author Stephen Brower
 */
public class BallDriver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        // create an array of Ball
        Ball[] ballList = new Ball[5];
        // each element should be null
        
        displayList(ballList,"List on declaration of array");
            
        // visit each element in array and instantiate
        // a new Ball object
        for (int i = 0; i < ballList.length; i++)
            ballList[i] = new Ball( (i+1)*10.0, i+5, i+10);

        displayList(ballList,"List after initialization of each element");
        
        // write a for loop here to visit each element in 
        // the array, and call at least one of the setters
        // for that object ( .setX and/or .setY and/or .setRadius
        for(int i = 0; i < ballList.length; i++)
        {
        	ballList[i].setX(i*6);
        	ballList[i].setRadius(i + 11.23);
        }
        
        displayList(ballList,"List after each object changed");
        
        /* // when Ball was first created, a default
        // constructor was added to Ball
        // initial instance Ball mySoccerBall = new Ball();
        Ball mySoccerBall = new Ball(3.4, 10, 20);
        
        // implicitly calls toString
        System.out.println(mySoccerBall);
        // test setters
        mySoccerBall.setRadius(13.1);
        mySoccerBall.setX(50);
        mySoccerBall.setY(75);
        // test getters
        System.out.println(mySoccerBall.getRadius());
        System.out.println(mySoccerBall.getX());
        System.out.println(mySoccerBall.getY()); */
    }
    
    public static void displayList(Ball[] ballList, String displayLabel)
    {
        System.out.println("\n" + displayLabel);
        for (int i = 0; i < ballList.length; i++)
            System.out.printf("%2d %s\n", i, ballList[i]);
        
    }
    
}
