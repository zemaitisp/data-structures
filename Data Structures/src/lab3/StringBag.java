package lab3;

/**
 * Used to create an array of mutable size that will store string
 * will accommodate any size and will expand if it needs to but will not shrink
 * can add and remove elements as needed
 * @author Peter
 *
 */
public class StringBag 
{
	private String[] data;
	private int numElements = 0;
	
	/**
	 *Will construct a StringBag with the given number of spots
	 * @param elements given number of spots in the bag
	 */
	public StringBag(int elements)
	{
		data = new String[elements];
	}
	
	/**
	 * No args constructor that will start at a size of 10
	 */
	public StringBag()
	{
		data = new String[10];
	}
	
	/**
	 * Will enlarge the array if you try to add more than it is capable of storing
	 * will double the array size each time it is called
	 */
	private void enlargeArray()
	{
		String[] newArray = new String[data.length * 2 + 1];
		for(int i = 0; i < numElements; i++)
		{
			newArray[i] = data[i];
		}
		data = newArray;
	}
	
	/**
	 * return the total length of the array
	 * @return total length of the array
	 */
	public int getCapacity()
	{
		return data.length;
	}
	
	/**
	 * will return the number of string currently in the array
	 * @return number of string currently in the array
	 */
	public int getSize()
	{
		return numElements;
	}
	
	/**
	 * will add a new element to the array, growing the bag if needed
	 * @param newElement new element to be added to the array
	 */
	public void add(String newElement)
	{
		if(numElements == data.length)
		{
			enlargeArray();
		}
		data[numElements] = newElement;
		numElements++;
	}
	
	/**
	 * Will attempt to find the index of target where ever it is in the array
	 * will return zero if it is not found
	 * @param target String you are searching for in the Stringbag
	 * @return -1 if not present or the index of where the first occurrence of the target is
	 */
	private int getTargetLocation(String target)
	{
		int index = -1;
		boolean doneSearching = false;
		for(int i = 0; i < numElements && !doneSearching; i++)
		{
			if(data[i].equalsIgnoreCase(target))
			{
				index = i;
				doneSearching = true;
			}
		}
		return index;
	}
	
	/**
	 * Will remove the first occurrence of target in the string bag and return true if a string
	 * was removed false otherwise
	 * @param target String to be removed from the Stringbag
	 * @return true if a String was removed false otherwise
	 */
	public boolean remove(String target)
	{	
		boolean successfullyRemoved;
		int index = getTargetLocation(target);
		if(index == -1)
		{
			successfullyRemoved = false;
		}
		else
		{
			for(int i = index; i < numElements; i++)
			{
				data[i] = data[i+1];
			}
			successfullyRemoved = true;
			numElements--;
		}
		return successfullyRemoved;
	}
	
	/**
	 * Will attempt to find if a target exists in a given Stringbag
	 * @param target String you are searching for in the Stringbag
	 * @return true if the target is in the Stringbag false otherwise
	 */
	public boolean exists(String target)
	{
		int index = getTargetLocation(target);
		boolean exists;
		if(index == -1)
			exists = false;
		else 
			exists = true;
		return exists;
	}
	
	/**
	 * Count the total number of times a target appears in a Stringbag
	 * @param target String you are searching for
	 * @return number of time a target appears in a Stringbag
	 */
	public int countOccurrences(String target)
	{
		int occurCount = 0;
		for(int i = 0; i < numElements; i++)
		{
			if(data[i].equalsIgnoreCase(target))
			{
				occurCount++;
			}
		}
		return occurCount;
	}
	
	/**
	 * Will format the output into a readable format
	 * @return 
	 */
	public String toString()
	{
		String toReturn = "";
		if(numElements == 0)
		{
			toReturn = "empty";
		}
		else
		{
			for(int i = 0; i < numElements; i++ )
			{
				toReturn = toReturn + data[i] + ", ";
			}
		}
		return toReturn;
	}
}