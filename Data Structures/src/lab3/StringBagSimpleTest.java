// STB - removed statement package lab03collectionclassnetbeansprojectspring2017;
package lab3;
/**
 *
 * unless you are commenting out a method call for testing, no need to change this file
 *
 * Lab 3 String Bag - tester for Lab 3
 *                                      SEE SAMPLE OUTPUT AT BOTTOM OF FILE
 * Programmed by    Stephen Brower
 * Inspired by  Michael Main
 * @author Stephen T. Brower<stephen.brower@raritanval.edu>
 */
public class StringBagSimpleTest
{
    /**
     * The main method is the program's starting point.
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // create a default sized bag
        StringBag defaultBag = new StringBag();

        // create another bag - fruitBag
        StringBag fruitBag = new StringBag(3);

        // display empty defaultBag
        System.out.print("\ndefaultBag upon startup: [" + defaultBag +"]");
        System.out.println("\nCapacity: " + defaultBag.getCapacity()
                + " Size: " + defaultBag.getSize());

        // display empty fruitBag
        System.out.print("\nfruitBag upon startup: [" + fruitBag +"]");
        System.out.println("\nCapacity: " + fruitBag.getCapacity()
                + " Size: " + fruitBag.getSize());

        // add fruit to fruitBag
        fruitBag.add("Pear");
        fruitBag.add("Plum");
        fruitBag.add("Peach");

        // display fruitBag after 3 adds
        System.out.print("\nfruitBag after 3 adds: [" + fruitBag +"]");
        System.out.println("\nCapacity: " + fruitBag.getCapacity()
                + " Size: " + fruitBag.getSize());

        // add 1 more fruit (fruitBag should grow)
        fruitBag.add("Mango");

        // show fruitBag after add/grow
        System.out.print("\nfruitBag bag after add/grow: [" + fruitBag +"]");
        System.out.println("\nCapacity: " + fruitBag.getCapacity()
                + " Size: " + fruitBag.getSize());

        // does banana exist in fruitBag?
        if (fruitBag.exists("banana"))
            System.out.println("\nYes!  There is a banana in the fruitBag!");
        else
            System.out.println("\nNo!  No banana in the fruitBag");

        // does plum exist in fruitBag?
        if (fruitBag.exists("plum"))
            System.out.println("Yes!  There is a plum in the fruitBag!");
        else
            System.out.println("No!  No plum in the fruitBag");

        // count some fruit in fruitBag
        System.out.println("\nNumber of banana in fruitBag: "
                + fruitBag.countOccurrences("banana"));
        System.out.println("Number of plum in fruitBag: "
                + fruitBag.countOccurrences("plum"));

        // removes

        // show fruitBag before call to remove()
        System.out.print("\nfruitBag before removing banana: [" + fruitBag +"]");
        System.out.println("\nCapacity: " + fruitBag.getCapacity()
                + " Size: " + fruitBag.getSize());

        // attempt to remove banana from fruitBag
        if (fruitBag.remove("banana"))
            System.out.println("\nWas able to remove banana from fruitBag.");
        else
            System.out.println("\nSorry! unable to remove banana from fruitBag!");

        // show fruitBag after call to remove()
        System.out.print("\nfruitBag after removing banana: [" + fruitBag +"]");
        System.out.println("\nCapacity: " + fruitBag.getCapacity()
                + " Size: " + fruitBag.getSize());


        // show fruitBag before call to remove()
        System.out.print("\nfruitBag before removing plum: [" + fruitBag +"]");
        System.out.println("\nCapacity: " + fruitBag.getCapacity()
                + " Size: " + fruitBag.getSize());

        // attempt to remove plum from fruitBag
        if (fruitBag.remove("plum"))
            System.out.println("\nWas able to remove plum from fruitBag.");
        else
            System.out.println("\nSorry! unable to remove plum from fruitBag!");

        System.out.print("\nfruitBag after removing plum: [" + fruitBag +"]");
        System.out.println("\nCapacity: " + fruitBag.getCapacity()
                + " Size: " + fruitBag.getSize());
    }
}
/*
Sample Run Should Be:
run:

defaultBag upon startup: [empty]
Capacity: 10 Size: 0

fruitBag upon startup: [empty]
Capacity: 3 Size: 0

fruitBag after 3 adds: [Pear, Plum, Peach, ]
Capacity: 3 Size: 3

fruitBag bag after add/grow: [Pear, Plum, Peach, Mango, ]
Capacity: 7 Size: 4

No!  No banana in the fruitBag
Yes!  There is a plum in the fruitBag!

Number of banana in fruitBag: 0
Number of plum in fruitBag: 1

fruitBag before removing banana: [Pear, Plum, Peach, Mango, ]
Capacity: 7 Size: 4

Sorry! unable to remove banana from fruitBag!

fruitBag after removing banana: [Pear, Plum, Peach, Mango, ]
Capacity: 7 Size: 4

fruitBag before removing plum: [Pear, Plum, Peach, Mango, ]
Capacity: 7 Size: 4

Was able to remove plum from fruitBag.

fruitBag after removing plum: [Pear, Mango, Peach, ]
Capacity: 7 Size: 3
BUILD SUCCESSFUL (total time: 0 seconds)

*/