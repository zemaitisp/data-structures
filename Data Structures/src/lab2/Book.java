package lab2;

/**
 * A class to store a book title and the year that book was published
 * @author Peter
 *
 */
public class Book 
{
	//To store the title of the book
	private String bookName;
	//To store the year the book was first published
	private int yearPublished;
	
	/**
	 * No args constructor for the book class
	 */
	public Book()
	{
		bookName = "Hitchhiker's Guide to the Galaxy";
		setYearPublished(1981);
	}
	/**
	 * Will create a book obj with the parameter bookName and yearPublished filled
	 * @param name name of the book
	 * @param published year the book was published
	 */
	public Book(String name, int published)
	{
		bookName = name;
		setYearPublished(published);
	}
	
	/**
	 * Will set the name of the book to a given string
	 * @param bookName new name of the book
	 */
	public void setBookName(String bookName)
	{
		this.bookName = bookName;
	}
	
	/**
	 * Will return the name of the book
	 * @return bookName 
	 */
	public String getBookName()
	{
		return bookName;
	}
	
	/**
	 * Will set the year the book was published to a given int
	 * @param yearPublished new year the book was published
	 */
	public void setYearPublished(int yearPublished) 
	{
		this.yearPublished = yearPublished;
	}
	
	/**
	 * Will return what year the book was published in
	 * @return year the book was published
	 */
	public int getYearPublished() 
	{
		return yearPublished;
	}
	
	/**
	 * To tell if two book objs are the same 
	 * @param comparing Book obj your are comparing to
	 * @return true if yearPublished and bookName are the same false otherwise
	 */
	public boolean equals(Book comparing)
	{
		return(this.getBookName().equals(comparing.getBookName()) && 
				this.getYearPublished() == comparing.getYearPublished());
	}
	
	/**
	 * Will return a properly formated string with the book name and the year published
	 */
	public String toString()
	{
		String message = "The name of the book is: "+ this.getBookName() + 
						"\n The year it was published was: " + this.getYearPublished() + "\n";
		return message;
	}
	
}
