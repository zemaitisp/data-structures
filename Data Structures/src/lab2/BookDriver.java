package lab2;
import java.util.*;
/**
 * Will demo the book class by allowing the user to imput a book title and the
 * year the book was published and then displaying that info to the user
 * @author Peter
 *
 */
public class BookDriver {
	/**
	 * Main method for the bookDriver
	 * Will ask the user for input and then display that input in a readable format
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Book b;
		
		//To store the user input for later
		String name;
		int yearPublished;
		
		//To get the user input
		System.out.println("Please enter your book name");
		name = scan.nextLine();
		System.out.println("Please enter the year that book was published");
		yearPublished = scan.nextInt();
		scan.nextLine();
	
		//Create a new obj with the users info
		b = new Book(name, yearPublished);
		System.out.println("The book name is: " + b.getBookName() +
							"\n The year the book was published was: " + b.getYearPublished());
	}

}
