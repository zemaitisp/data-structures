package lab7;

public class BTNode <E> 
{
	private BTNode<E> left;
	private BTNode<E> right;
	private E data;
	
	/**
	 * Constructor to create a new Binary Tree Node which will hold one piece of data and two links
	 * @param left new BTNode to the left of this one, less than this one
	 * @param right new BTNode to the right of this one, greater than this one
	 * @param element data to be stored in this node
	 */
	public BTNode(BTNode<E> left, BTNode<E> right, E element)
	{
		this.left = left;
		this.right = right;
		data = element;
	}
	
	/**
	 * Will get the BTNode to the left of the current one
	 * @return BTNode to the left
	 */
	public BTNode<E> getLeft()
	{
		return left;
	}
	
	/**
	 * Will get the BTNode to the right of the current one
	 * @return BTNode to the right
	 */
	public BTNode<E> getRight()
	{
		return right;
	}
	
	public E getRightMostData()
	{
		E dataToReturn = null;
		BTNode<E> cursor = null;
		if(this != null)
		{
			cursor = this;
			while(cursor.getRight() != null)
			{
				cursor = cursor.getRight();
			}
			dataToReturn = cursor.getData();
		}
		return dataToReturn;
	}
	
	public BTNode<E> removeRightMost()
	{
		BTNode<E> toReturn = null;
		BTNode<E> cursor = null;
		if(this != null)
		{
			cursor = this;
			while(cursor.getRight() != null)
			{
				cursor = cursor.getRight();
			}
			toReturn = cursor;
			cursor = null;
		}
		return toReturn;
	}
	
	/**
	 * Will return the data held by the current BTNode
	 * @return data held by this node
	 */
	public E getData()
	{
		return data;
	}
	
	/**
	 * Will allow the user to set the BTNode to the left of the current BTNode
	 * @param newLink new link to the left of the current BTNode
	 */
	public void setLeft(BTNode<E> newLink)
	{
		left = newLink;
	}
	
	/**
	 * Will allow the user to set the BTNode to the right of the current BTNode
	 * @param newLink new link to the right of the current BTNode
	 */
	public void setRight(BTNode<E> newLink)
	{
		right = newLink;
	}
	
	/**
	 * Will allow the user to set the data of the current BTNode
	 * @param newData data that the current BTNode will hold
	 */
	public void setData(E newData)
	{
		data = newData;
	}
}
