package lab5;

/**
*
* unless you are commenting out a method call for testing, no need to change this file
*
* Lab 5 LinkedBag/Node - tester for Lab 5
*
* Programmed by    Stephen Brower
* Inspired by  Michael Main
* @author Stephen T. Brower<stephen.brower@raritanval.edu>
*/
public class LinkedBagSimpleTest
{
   /**
    * The main method is the program's starting point.
    * @param args the command line arguments
    */
   public static void main(String[] args)
   {
       // create bag of String  - fruitBag
       LinkedBag<String> fruitBag = new LinkedBag<String>();
       // create bag of Car - carBag
       LinkedBag<Car> carBag = new LinkedBag<Car>();

       // variables
       String stringToTest;
       Car carToTest;

       // display empty fruitBag
       displayBagAndSize(fruitBag,"fruitBag upon startup");
       // display empty carBag
       displayBagAndSize(carBag,"carBag upon startup");

       // add fruit to fruitBag
       addStringToBag(fruitBag, "Pear");
       addStringToBag(fruitBag, "Plum");
       addStringToBag(fruitBag, "Watermellon");
       addStringToBag(fruitBag, "Mango");
       addStringToBag(fruitBag, "Apple");
       addStringToBag(fruitBag, "Nectarine");
       addStringToBag(fruitBag, "Cherry");

       // display fruitBag after 7 adds
       displayBagAndSize(fruitBag,"\nfruitBag after 7 adds");
       
       System.out.println(fruitBag.toString());

       addCarToBag(carBag, new Car(2017,"Honda Accord") );
       addCarToBag(carBag, new Car(2016,"Camry") );
       addCarToBag(carBag, new Car(2017,"Explorer") );
       addCarToBag(carBag, new Car(2016,"Zoom Zoom") );
       addCarToBag(carBag, new Car(2017,"Acura") );
       addCarToBag(carBag, new Car(2016,"Neon") );
       addCarToBag(carBag, new Car(1965,"Shelby Mustang GT 350") );
       addCarToBag(carBag, new Car(2016,"zOom zoOM") );

       // display carBag after 8 adds
       displayBagAndSize(carBag,"\ncarBag after 8 adds");


       // exists - String

       stringToTest = "Banana";
       // does stringToTest exist in fruitBag?
       if (fruitBag.exists(stringToTest))
           System.out.println("\nYes!  There is a " + stringToTest + " in the fruitBag!");
       else
           System.out.println("\nNo!  No " + stringToTest + " in the fruitBag");

       stringToTest = "Nectarine";
       // does stringToTest exist in fruitBag?
       if (fruitBag.exists(stringToTest))
           System.out.println("Yes!  There is a " + stringToTest + " in the fruitBag!");
       else
           System.out.println("No!  No " + stringToTest + " in the fruitBag");

       stringToTest = "Watermellon";
       // does stringToTest exist in fruitBag?
       if (fruitBag.exists(stringToTest))
           System.out.println("Yes!  There is a " + stringToTest + " in the fruitBag!");
       else
           System.out.println("No!  No " + stringToTest + " in the fruitBag");

       // exists - Car

       carToTest = new Car(2017,"Expedition");
       // does carToTest exist in carBag?
       if (carBag.exists(carToTest))
           System.out.println("\nYes!  There is a " + carToTest + " in the carBag!");
       else
           System.out.println("\nNo!  No " + carToTest + " in the carBag");

       carToTest = new Car(2017,"Honda Accord");
       // does carToTest exist in carBag?
       if (carBag.exists(carToTest))
           System.out.println("Yes!  There is a " + carToTest + " in the carBag!");
       else
           System.out.println("No!  No " + carToTest + " in the carBag");

       carToTest = new Car(1965,"Shelby Mustang GT 350");
       // does carToTest exist in carBag?
       if (carBag.exists(carToTest))
           System.out.println("Yes!  There is a " + carToTest + " in the carBag!");
       else
           System.out.println("No!  No " + carToTest + " in the carBag");


       // remove       String

       //                          Banana
       stringToTest = "Banana";
       // show fruitBag before call to remove()
       displayBagAndSize(fruitBag,"\nfruitBag before removing " + stringToTest);

       // attempt to remove stringToTest from fruitBag
       if (fruitBag.remove(stringToTest))
           System.out.println("Was able to remove " + stringToTest + " from fruitBag.");
       else
           System.out.println("Sorry! unable to remove " + stringToTest + " from fruitBag!");

       // show fruitBag after call to remove()
       displayBagAndSize(fruitBag,"fruitBag after removing " + stringToTest);


       //                          Nectarine
       stringToTest = "Nectarine";
       // show fruitBag before call to remove()
       displayBagAndSize(fruitBag,"\nfruitBag before removing " + stringToTest);

       // attempt to remove stringToTest from fruitBag
       if (fruitBag.remove(stringToTest))
           System.out.println("Was able to remove " + stringToTest + " from fruitBag.");
       else
           System.out.println("Sorry! unable to remove " + stringToTest + " from fruitBag!");

       // show fruitBag after call to remove()
       displayBagAndSize(fruitBag,"fruitBag after removing " + stringToTest);


       //                          Watermellon
       stringToTest = "Watermellon";
       // show fruitBag before call to remove()
       displayBagAndSize(fruitBag,"\nfruitBag before removing " + stringToTest);

       // attempt to remove stringToTest from fruitBag
       if (fruitBag.remove(stringToTest))
           System.out.println("Was able to remove " + stringToTest + " from fruitBag.");
       else
           System.out.println("Sorry! unable to remove " + stringToTest + " from fruitBag!");

       // show fruitBag after call to remove()
       displayBagAndSize(fruitBag,"fruitBag after removing " + stringToTest);


       // remove       Car

       carToTest = new Car(2017,"Expedition");
       // show carBag before call to remove()
       displayBagAndSize(carBag,"\ncarBag before removing <" + carToTest + ">");

       // attempt to remove stringToTest from carBag
       if (carBag.remove(carToTest))
           System.out.println("Was able to remove " + carToTest + " from carBag.");
       else
           System.out.println("Sorry! unable to remove " + carToTest + " from carBag!");

       // show carBag after call to remove()
       displayBagAndSize(carBag,"carBag after removing <" + carToTest + ">");

       carToTest = new Car(2017,"Honda Accord");
       // show carBag before call to remove()
       displayBagAndSize(carBag,"\ncarBag before removing <" + carToTest + ">");

       // attempt to remove stringToTest from carBag
       if (carBag.remove(carToTest))
           System.out.println("Was able to remove " + carToTest + " from carBag.");
       else
           System.out.println("Sorry! unable to remove " + carToTest + " from carBag!");

       // show carBag after call to remove()
       displayBagAndSize(carBag,"carBag after removing <" + carToTest + ">");

       carToTest = new Car(1965,"Shelby Mustang GT 350");
       // show carBag before call to remove()
       displayBagAndSize(carBag,"\ncarBag before removing <" + carToTest + ">");

       // attempt to remove stringToTest from carBag
       if (carBag.remove(carToTest))
           System.out.println("Was able to remove " + carToTest + " from carBag.");
       else
           System.out.println("Sorry! unable to remove " + carToTest + " from carBag!");

       // show carBag after call to remove()
       displayBagAndSize(carBag,"carBag after removing <" + carToTest + ">");


   }

   public static void displaySize(LinkedBag bagToDisplay, String label)
   {
       System.out.println(label+"\tSize: " + bagToDisplay.getSize());
   }

   public static void displayBagAndSize(LinkedBag bagToDisplay, String label)
   {
       System.out.print(label + ": [" + bagToDisplay +"]");
       System.out.println("\tSize: " + bagToDisplay.getSize());
   }

   public static void addStringToBag(LinkedBag<String> bagToAddTo,
                                       String stringToAdd )
   {
       bagToAddTo.add(stringToAdd);
       displaySize(bagToAddTo,"added [" + stringToAdd + "]...");

   }

   public static void addCarToBag(LinkedBag<Car> bagToAddTo,
                                       Car carToAdd )
   {
       bagToAddTo.add(carToAdd);
       displaySize(bagToAddTo,"added [" + carToAdd + "]...");

   }

}
/*


*/