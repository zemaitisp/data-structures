package lab5;
/**
 * 
 * Will hold a list of nodes of the same data type. Any data type can be used as long as it extends comparable
 * @author Peter Zemaitis 
 *
 * @param <E> data type you are working with
 */
public class LinkedBag<E extends Comparable<E>> 
{
	private Node<E> head;
	private int numElements;
	
	/**
	 * Will set head to null and numElements to zero(0)
	 */
	public LinkedBag()
	{
		head = null;
		numElements = 0;
	}
	
	/**
	 * Will retrun the number of nodes in the list
	 * @return number of nodes in the list
	 */
	public int getSize()
	{
		return numElements;
	}
	
	/**
	 * Will add a node to the list using the compareTo method to decide where in the list it should be added
	 * @param element data you want added to the list
	 */
	public void add(E element)
	{
		boolean placed = false;
		Node<E> cursor = head;
		Node<E> prev = null;
		if(cursor == null)
		{
			head = new Node<E>(element, null);
			placed = true;
		}
		else
		{
			
			while(cursor != null && !placed)
			{
				if(element.compareTo(cursor.getData()) <= 0)
				{
					if(prev == null)
					{
						head = new Node<E>(element, head);
					}
					else
					{
						prev.setLink(new Node<E> (element, cursor));
					}
					placed = true;
				}
					prev = cursor;
					cursor = cursor.getLink();
			}
		}
		if(!placed)
			prev.setLink(new Node<E>(element, null));
		
		numElements++;
	}
	
	/**
	 * Will attempt to find the given data in the list. If it exists it will return true, false otherwise
	 * @param target data you wish to find in the list
	 * @return true if data is found, false otherwise
	 */
	public boolean exists(E target)
	{
		boolean found = false;
		Node<E> cursor = head;
		while(cursor != null && !found)
		{
			if(cursor.getData().equals(target))
			{
				found = true;
			}
			else
			{
				cursor = cursor.getLink();
			}
		}
		return found;
	}
	
	/**
	 * Will attempt to remove a data element from the list. Will return true if it removed an element false otherwise
	 * @param target data you want removed from the list
	 * @return true if data was removed, false otherwise
	 */
	public boolean remove(E target)
	{
		boolean removed = false;
		Node<E> cursor = head;
		Node<E> prev = null;
		if(head != null)
		{
			while(cursor != null && !removed)
			{
				if(cursor.getData().equals(target))
				{
					if(prev == null)
					{
						head = head.getLink();
					}
					else
					{
						prev.setLink(cursor.getLink());
					}
					numElements--;
					removed = true;
				}
				prev = cursor;
				cursor = cursor.getLink();
			}
		}
		return removed;
	}
	
	/**
	 * Should not be used. Use iterator instead
	 * @return String containing all data in the list, formated in a readable manner
	 */
	public String toString()
	{
		String toReturn = "";
		Node<E> cursor = head;
		while(cursor != null)
		{
			toReturn = toReturn + cursor.getData() + ", ";
			cursor = cursor.getLink();
		}
		return toReturn;
	}
	
	/**
	 * Will return a Lister obj, that can be used to manipulate the node list
	 * @return
	 */
	public Lister<E> iterator()
	{
		return new Lister<E>(head);
	}
	
	
}
