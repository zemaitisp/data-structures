package lab4;

/**
 * Uses StringNodes to make a linked list with given data
 * @author Peter Zemaitis
 *
 */
public class StringLinkedBag
{
	private StringNode head;
	private int numElement;
	
	/**
	 * No args for the StringLinkedBag that initializes numElements to zero and head to null
	 */
	public StringLinkedBag()
	{
		head = null;
		numElement = 0;
	}
	
	/**
	 * returns the total number of nodes in the list
	 * @return number of nodes in the list
	 */
	public int getSize()
	{
		return numElement;
	}
	
	/**
	 * Will add a given element to the linked list
	 * @param element data added to the list
	 */
	public void add(String element)
	{
		head = new StringNode(element,head);
		numElement++;
	}
	
	/**
	 * checks to see if a given value is present in the linked list
	 * @param target data we are checking if it is in the list
	 * @return true if found in the list false otherwise
	 */
	public boolean exists(String target)
	{
		StringNode cursor = head;
		boolean found = false;
		while (cursor != null && !found)
		{
			if(cursor.getData().equalsIgnoreCase(target))
			{
				found = true;
			}
			else
			{
				cursor = cursor.getLink();
			}
		}
		return found;
	}
	
	/**
	 * Will remove the first node with a given target
	 * @param target data to be removed
	 * @return true if able to be removed false otherwise
	 */
	public boolean remove(String target)
	{
		StringNode cursor = head;
		boolean found = false;
		while (cursor != null && !found)
		{
			if(cursor.getData().equalsIgnoreCase(target))
			{
				found = true;
			}
			else
			{
				cursor = cursor.getLink();
			}
		}
		if(found)
		{
			cursor.setData(head.getData());
			head = head.getLink();
			numElement--;
		}
		return found;
	}
	
	/**
	 * Returns a string that is readable to the user
	 * @return String that is readable by the user
	 */
	public String toString()
	{
		String toReturn = "";
		StringNode cursor;
		if(numElement == 0)
		{
			toReturn = "Empty";
		}
		else
		{
			for(cursor = head; cursor != null; cursor = cursor.getLink())
			{
				toReturn = toReturn + cursor.getData();
				if(cursor.getLink() != null)
				{
					toReturn = toReturn + ", ";
				}
			}
		}
		return toReturn;
	}
}
