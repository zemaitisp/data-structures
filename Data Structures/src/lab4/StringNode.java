package lab4;

/**
 * Will store a single String and another stringNode
 * @author Peter Zemaitis
 *
 */
public class StringNode 
{
	private String data;
	private StringNode link;
	
	/**
	 * Will intialize the StringNode with the given data and a link to the next StringNode
	 * @param initalData data we are storing in the stringNode
	 * @param initalLink link to the nextString node in the LinkedList
	 */
	public StringNode(String initalData, StringNode initalLink)
	{
		data = initalData;
		link = initalLink;
	}
	
	/**
	 * Return String stored in the node
	 * @return string stored in this node
	 */
	public String getData()
	{
		return data;
	}
	
	/**
	 * return link to the next node in the list
	 * @return link to the next node
	 */
	public StringNode getLink()
	{
		return link;
	}
	
	/**
	 * set the String in this node to the given value
	 * @param newData new string data we are replacing 
	 */
	public void setData(String newData)
	{
		data = newData;
	}
	
	/**
	 * setting the next link to a new location
	 * @param newLink new location for the link
	 */
	public void setLink(StringNode newLink)
	{
		link = newLink;
	}
}
